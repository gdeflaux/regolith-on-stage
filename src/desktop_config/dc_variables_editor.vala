using Gee;

public class Ros.VariablesEditor : Gtk.Box {
  public Gtk.Label            variable_name;
  public Gtk.Entry            variable_value;
  public Gtk.Label            is_macro;
  public Gtk.Label            is_user_defined;
  public Gtk.Label            macro_value;
  public Gtk.Label            default_value;
  public Gtk.Button           macro_button;
  public Gtk.Button           save_button;
  public Gtk.Button           reset_button;
  public Gtk.Button           cancel_button;
  public Ros.DesktopConfigBox desktop_config;

  public VariablesEditor(Ros.DesktopConfigBox desktop_config) {
    orientation = Gtk.Orientation.VERTICAL;
    spacing = 5;
    margin = 5;
    width_request = 300;
    this.desktop_config = desktop_config;

    // Editor title
    var editor_label = new Gtk.Label("");
    Ros.StyleHelper.add_class(editor_label, "desktop_config_header");
    editor_label.label = "Variable Properties";
    editor_label.halign = Gtk.Align.START;
    editor_label.margin_bottom = 10;
    this.pack_start(editor_label, false, false, 0);


    // Variable name
    var variable_name_label = new Gtk.Label("");
    variable_name_label.set_markup("<b>Name</b>");
    variable_name_label.halign = Gtk.Align.START;
    variable_name = new Gtk.Label("");
    variable_name.max_width_chars = 30;
    variable_name.set_line_wrap(true);
    variable_name.wrap_mode = Pango.WrapMode.CHAR;
    variable_name.halign = Gtk.Align.START;
    variable_name.margin_bottom = 10;
    this.pack_start(variable_name_label, false, false, 0);
    this.pack_start(variable_name, false, false, 0);

    // Default value
    var default_value_label = new Gtk.Label("");
    default_value_label.set_markup("<b>Default Value</b>");
    default_value_label.halign = Gtk.Align.START;
    default_value = new Gtk.Label("");
    default_value.max_width_chars = 30;
    default_value.set_line_wrap(true);
    default_value.wrap_mode = Pango.WrapMode.CHAR;
    default_value.halign = Gtk.Align.START;
    default_value.margin_bottom = 10;
    default_value.selectable = true;
    this.pack_start(default_value_label, false, false, 0);
    this.pack_start(default_value, false, false, 0);

    // Current value
    var variable_value_label = new Gtk.Label("");
    variable_value_label.set_markup("<b>Current Value</b>");
    variable_value_label.halign = Gtk.Align.START;
    this.pack_start(variable_value_label, false, false, 0);

    var value_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
    value_box.margin_bottom = 10;
    this.pack_start(value_box, false, false, 0);

    variable_value = new Gtk.Entry();
    Ros.StyleHelper.add_class(variable_value, "no_right_radius");
    variable_value.key_release_event.connect((key) => {
      save_button.sensitive = true;
      return false;
    });
    value_box.pack_start(variable_value, true, true, 0);

    // Macro Button
    macro_button = new Gtk.Button.with_label("#");
    macro_button.tooltip_text = "Insert a macro";
    Ros.StyleHelper.add_class(macro_button, "no_left_radius");
    macro_button.clicked.connect(() => {
      var dialog = new Ros.MacrosWindow(desktop_config, variable_value);
      dialog.present();
    });
    value_box.pack_start(macro_button, false, true, 0);

    // Is Macro
    var is_macro_label = new Gtk.Label("");
    is_macro_label.set_markup("<b>Is Macro</b>");
    is_macro_label.halign = Gtk.Align.START;
    is_macro = new Gtk.Label("");
    is_macro.halign = Gtk.Align.START;
    is_macro.margin_bottom = 10;
    this.pack_start(is_macro_label, false, false, 0);
    this.pack_start(is_macro, false, false, 0);

    // Macro value
    var macro_label = new Gtk.Label("");
    macro_label.set_markup("<b>Macro Value</b>");
    macro_label.halign = Gtk.Align.START;
    macro_value = new Gtk.Label("");
    macro_value.max_width_chars = 30;
    macro_value.set_line_wrap(true);
    macro_value.wrap_mode = Pango.WrapMode.CHAR;
    macro_value.halign = Gtk.Align.START;
    macro_value.selectable = true;
    macro_value.margin_bottom = 10;
    this.pack_start(macro_label, false, false, 0);
    this.pack_start(macro_value, false, false, 0);

    // Is user defined
    var is_user_defined_label = new Gtk.Label("");
    is_user_defined_label.set_markup("<b>Is Custom</b>");
    is_user_defined_label.halign = Gtk.Align.START;
    is_user_defined = new Gtk.Label("");
    is_user_defined.halign = Gtk.Align.START;
    is_user_defined.margin_bottom = 10;
    this.pack_start(is_user_defined_label, false, false, 0);
    this.pack_start(is_user_defined, false, false, 0);

    // Save button
    save_button = new Gtk.Button.with_label("Save Changes");
    Ros.StyleHelper.add_class(save_button, "suggested-action");
    save_button.clicked.connect(() => {
      update_tree_store("save");
    });
    this.pack_start(save_button, false, false, 0);

    // Reset button
    reset_button = new Gtk.Button.with_label("Reset to Default");
    reset_button.clicked.connect(() => {
      update_tree_store("reset");
    });
    this.pack_start(reset_button, false, false, 0);

    // Cancel button
    cancel_button = new Gtk.Button.with_label("Discard Changes");
    cancel_button.clicked.connect(() => {
      toggle_editor("OFF");
      desktop_config.variables_tree.get_selection().unselect_all();
    });
    this.pack_start(cancel_button, false, false, 0);

    // Separator
    this.pack_start(new Gtk.Separator(Gtk.Orientation.HORIZONTAL), false, false, 0);

    // Write button + Refresh checkbox
    var refresh_checkbox = new Gtk.CheckButton.with_label("Refresh look on write");
    var write_button = new Gtk.Button.with_label("Write Config File");
    Ros.StyleHelper.add_class(write_button, "suggested-action");
    write_button.valign = Gtk.Align.END;
    write_button.clicked.connect(() => {
      var d = new Gtk.MessageDialog(null, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING, Gtk.ButtonsType.OK_CANCEL, "Are you sure?");
      d.secondary_text = "This action will <b>overwrite</b> existing custom variables.";
      d.secondary_use_markup = true;
      if (d.run() == Gtk.ResponseType.OK) {
        desktop_config.parser_manager.to_file(desktop_config.variables, desktop_config.macros);
        if (refresh_checkbox.active) {
          Process.spawn_command_line_sync ("/usr/bin/regolith-look refresh");
        }
      }
      d.destroy();
    });
    this.pack_start(write_button, true, true, 0);
    this.pack_start(refresh_checkbox, false, false, 0);

    // Other
    update_i3bar_icon_font(); // Update CSS for specific widgets
    toggle_editor("OFF"); // No variable is selected at startup

    // Help
    desktop_config.help.register_step("variables_editor", new Ros.HelpStep(this, "The left pane shows the properties of the selected Xresource definition."));
    desktop_config.help.register_step("variable_name", new Ros.HelpStep(variable_name, "This is the name of the Xresource definition."));
    desktop_config.help.register_step("variable_default", new Ros.HelpStep(default_value, "This is the default value. Is sourced from the look files or your <tt>~/.Xresources-regolith</tt> file."));
    desktop_config.help.register_step("reset_button", new Ros.HelpStep(reset_button, "You can restore the default value any anytime clicking the <tt>Reset to Default</tt> button."));
    desktop_config.help.register_step("variable_value", new Ros.HelpStep(variable_value, "This is where you can customize the value of the Xresrouce defintion."));
    desktop_config.help.register_step("macro_button", new Ros.HelpStep(macro_button, "Open the macro explorer to browse and reuse the macros defined in the system Look files so you can keep things DRY."));
    desktop_config.help.register_step("save_button", new Ros.HelpStep(save_button, "Once you are done, don't forget to save your changes by clicking the <tt>Save Changes</tt> button."));
    desktop_config.help.register_step("is_user_defined", new Ros.HelpStep(is_user_defined, "If the value you assign to a Xresource is different from the <i>default value</i> it is considered as a <i>custom</i> Xresource definition."));
    desktop_config.help.register_step("is_macro", new Ros.HelpStep(is_macro, "If the value saved to the Xresource is a macro used in the look files it is mentioned here."));
    desktop_config.help.register_step("macro_value", new Ros.HelpStep(macro_value, "In which case the actual value of the macro is displayed for convenience."));
    desktop_config.help.register_step("cancel_button", new Ros.HelpStep(cancel_button, "You can click the <i>Discard Changes</i> button anytime to discard any unsaved changes."));
    desktop_config.help.register_step("write_button", new Ros.HelpStep(write_button, "Once you are done you can click the <tt>Write Config File</tt> button to save your changes and refresh to see your new look!"));
  }

  public void update_tree_store(string action) {
    Gtk.TreeIter  it_sort, it_filter, it_store;
    Gtk.TreeModel data;
    string        variable_key;

    // Get variable key from selected row in tree
    desktop_config.variables_tree.get_selection().get_selected(out data, out it_sort);
    data.get(it_sort, 0, out variable_key);
    var vv = desktop_config.variables[variable_key];

    // Perform action specific stuff
    if (action == "reset") {
      vv.reset(desktop_config.macros);
      variable_value.text = vv.value;
    } else { // action == "save"
      vv.update(variable_value.text, desktop_config.macros);
    }

    // Update editor labels
    if (vv.is_macro) {
      macro_value.label = desktop_config.macros[vv.value];
      is_macro.label = "Yes";
    } else {
      macro_value.label = "";
      is_macro.label = "No";
    }
    is_user_defined.label = vv.is_user_defined ? "Yes" : "No";

    // Update tree
    var m = vv.is_macro ? "✓" : "";
    var u = vv.is_user_defined ? "✓" : "";
    desktop_config.variables_tree.sort_model.convert_iter_to_child_iter(out it_filter, it_sort);
    desktop_config.variables_tree.filter_model.convert_iter_to_child_iter(out it_store, it_filter);
    desktop_config.variables_tree.store.set(it_store, 1, vv.value, 2, m, 3, u);

    // Update widget icon font if font is modified
    if (variable_key == "i3-wm.bar.font") {
      update_i3bar_icon_font();
      desktop_config.variables_tree.update_i3bar_icon_font();
    }

    save_button.sensitive = false;
  }

  public void update_i3bar_icon_font() {
    var css_provider = desktop_config.get_i3bar_icon_font_css_provider();

    // Variable value Entry
    var css_context = Ros.StyleHelper.add_class(variable_value, "icon_font");
    css_context.add_provider(css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);

    // Macro value Label
    css_context = Ros.StyleHelper.add_class(macro_value, "icon_font");
    css_context.add_provider(css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);
  }

  public void toggle_editor(string state) {
    bool is_sensitive = (state == "ON");

    save_button.sensitive = false;
    reset_button.sensitive = is_sensitive;
    cancel_button.sensitive = is_sensitive;
    variable_value.sensitive = is_sensitive;
    macro_button.sensitive = is_sensitive;


    if (!is_sensitive) {
      clear_widgets();
    }
  }

  public void clear_widgets() {
    variable_name.label = "";
    variable_value.text = "";
    default_value.label = "";
    is_macro.label = "";
    macro_value.label = "";
    is_user_defined.label = "";
  }
}
