public class Ros.BlockList : Gtk.Box {
  public Gtk.ListBox            block_list;
  public string                 search_cl = "apt search ^i3xrocks-";
  public Array<Ros.I3BlockData> block_data;
  public Ros.BlockListHeader    installed_header;
  public Ros.BlockListHeader    available_header;
  public Ros.BarPreview         bar_preview;

  public BlockList(Ros.I3Blocks parent) {
    orientation = Gtk.Orientation.VERTICAL;
    spacing = 5;
    margin = 10;
    bar_preview = parent.bar_preview;
    installed_header = new Ros.BlockListHeader("Installed");
    available_header = new Ros.BlockListHeader("Available");

    var heading = new Gtk.Label(null);
    heading.halign = Gtk.Align.START;
    Ros.StyleHelper.add_class(heading, "i3blocks_header");
    heading.label = "List of i3Blocks";
    pack_start(heading, false, false, 0);

    block_data = get_blocks_data();

    block_list = new Gtk.ListBox();
    block_list.set_sort_func(sort_list);
    block_list.set_header_func(list_header);
    block_list.set_selection_mode(Gtk.SelectionMode.NONE);
    block_list.halign = Gtk.Align.CENTER;
    block_list.width_request = 500;

    var scroll_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
    pack_start(scroll_box, true, true, 0);
    var scrolled_window = new Gtk.ScrolledWindow(null, null);
    scroll_box.pack_start(scrolled_window, true, true, 0);
    scrolled_window.propagate_natural_width = true;
    scrolled_window.halign = Gtk.Align.CENTER;
    scrolled_window.add(block_list);

    for (int i = 0; i < block_data.length; i++) {
      var row = new Ros.BlockRow(block_data.index(i), bar_preview);
      block_list.add(row);
    }

    parent.help.register_step("block_list", new Ros.HelpStep(block_list, "This is the list of blocks. Both installed and available blocks are shown."));
    var r = block_list.get_row_at_index(0) as Ros.BlockRow;
    parent.help.register_step("row", new Ros.HelpStep(r, "Each row represents a block that you can install or remove"));
    parent.help.register_step("info", new Ros.HelpStep(r.icon, "Hover this icon to get a short description of the block."));
    parent.help.register_step("switch", new Ros.HelpStep(r.blk_switch, "Click in the switch to install or remove a block. The block will move to the corresponding section of the list."));
  }

  private int sort_list(Gtk.ListBoxRow _r1, Gtk.ListBoxRow _r2) {
    var r1 = _r1 as Ros.BlockRow;
    var r2 = _r2 as Ros.BlockRow;

    if (r1.block.installed_to_be && !r2.block.installed_to_be) {
      return -1;
    }

    if (!r1.block.installed_to_be && r2.block.installed_to_be) {
      return 1;
    }

    if (r1.block.name < r2.block.name) {
      return -1;
    } else {
      return 1;
    }

    return 0;
  }

  private void list_header(Gtk.ListBoxRow _row, Gtk.ListBoxRow? _before) {
    var row = _row as Ros.BlockRow;
    var before = _before as Ros.BlockRow;

    if (row.get_index() == 0) {
      row.set_header(installed_header);
    } else if (!row.block.installed_to_be && before.block.installed_to_be) {
      row.set_header(available_header);
    } else {
      row.set_header(null);
    }
  }

  private Array<Ros.I3BlockData> get_blocks_data() {
    var blocks = new Array<Ros.I3BlockData>();
    var cmd_output = "";

    Process.spawn_command_line_sync (search_cl, out cmd_output); // handle errors

    var blocks_list = cmd_output.split("\n");
    Regex regex = new Regex ("^i3xrocks-(.*)/(.*)");

    for(int i = 0; i < blocks_list.length; i++) {
      var s = regex.split(blocks_list[i]);

      if (s.length == 4) {
        var name = s[1];
        var installed = s[2].contains("[installed]") || s[2].contains("[upgradable");
        var desc = blocks_list[i + 1].strip();
        blocks.append_val(new I3BlockData(name, installed, desc));
        i += 1; // skip the description line
      }
    }

    return blocks;
  }
}

public class Ros.BlockListHeader : Gtk.Label {
  public BlockListHeader(string label) {
    set_markup("<b>" + label + "</b>");
    halign = Gtk.Align.START;
    margin = 5;
  }
}

public class Ros.BlockRow : Gtk.ListBoxRow {
  public Ros.I3BlockData  block;
  public Gtk.Switch       blk_switch;
  public Gtk.Image        icon;

  public BlockRow(Ros.I3BlockData b, Ros.BarPreview bar_preview) {
    block = b;
    block.row = this;

    var box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 5);
    box.margin = 10;
    this.add(box);

    // Block name
    var block_name = new Gtk.Label(block.name);
    block_name.halign = Gtk.Align.START;
    box.pack_start(block_name, false, true, 0);

    // Icon
    var icon_box = new Gtk.EventBox();
    icon_box.halign = Gtk.Align.END;
    icon_box.set_events(Gdk.EventMask.ENTER_NOTIFY_MASK);// | Gdk.EventMask.LEAVE_NOTIFY_MASK
    icon = new Gtk.Image.from_icon_name("help-faq", Gtk.IconSize.MENU);
    icon_box.add(icon);

    var desc_popover  = new Gtk.Popover(icon);
    desc_popover.position = Gtk.PositionType.RIGHT;
    var desc_label = new Gtk.Label(block.description);
    desc_label.margin = 5;
    desc_popover.add(desc_label);

    icon_box.enter_notify_event.connect(() => {
      desc_popover.show_all();
      desc_popover.popup();
      return true;
    });
    box.pack_start(icon_box, false, true, 0);

    // switch
    blk_switch = new Gtk.Switch();
    blk_switch.halign = Gtk.Align.END;
    blk_switch.state = block.installed_init;
    blk_switch.notify["active"].connect(() => {
      block.installed_to_be = blk_switch.state;
      changed();
      bar_preview.preview(block.name);
    });
    box.pack_start(blk_switch, true, true, 0);
  }
}


public class Ros.I3BlockData {
  public string       name;
  public bool         installed_init;
  public bool         installed_to_be;
  public string       description;
  public Ros.BlockRow row;

  public I3BlockData (string n, bool i, string d) {
    name = n;
    installed_init = i;
    installed_to_be = i;
    description = d;
  }

  public void to_s() {
    stdout.printf("Name: %s\n", name);
    stdout.printf("Installed: %s\n", installed_init ? "Yes" : "No");
    stdout.printf("Description: %s\n", description);
  }
}
