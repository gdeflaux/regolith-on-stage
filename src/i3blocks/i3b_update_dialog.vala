public class Ros.I3BlockUpdateDialog : Gtk.Window {
  public Ros.ApplicationWindow      parent_window;
  public Ros.BlockList              list;
  public Array<Ros.I3BlockData>     blocks;
  public Array<Ros.I3BlockData>     install_blocks;
  public Array<Ros.I3BlockData>     remove_blocks;
  public Ros.I3BlocksInfoLabel      install_label;
  public Ros.I3BlocksInfoLabel      remove_label;
  public Ros.I3BlocksInfoLabel      refresh_label;

  public I3BlockUpdateDialog(Ros.ApplicationWindow  parent_window, Ros.BlockList list) {
    this.blocks = list.block_data;
    this.list = list;
    this.parent_window = parent_window;
    style_window();

    // Split install and remove blocks
    install_blocks = new Array<I3BlockData>();
    remove_blocks = new Array<I3BlockData>();
    for (int i = 0; i < blocks.length; i++) {
      var block = blocks.index(i);

      if (block.installed_init != block.installed_to_be) {
        if (block.installed_to_be) {
          install_blocks.append_val(block);
        } else {
          remove_blocks.append_val(block);
        }
      }
    }

    var box = new Gtk.Box(Gtk.Orientation.VERTICAL, 15);
    box.margin = 10;
    add(box);

    if (install_blocks.length > 0) {
      install_label = new Ros.I3BlocksInfoLabel.with_blocks("Blocks to be installed:", install_blocks, "list-add-symbolic");
      box.pack_start(install_label, false, false, 0);
    }

    if (remove_blocks.length > 0) {
      remove_label = new Ros.I3BlocksInfoLabel.with_blocks("Blocks to be removed:", remove_blocks, "list-remove-symbolic");
      box.pack_start(remove_label, false, false, 0);
    }

    if ((install_blocks.length > 0) || (remove_blocks.length > 0)) {
      refresh_label = new Ros.I3BlocksInfoLabel("Refresh desktop.", "emblem-synchronizing-symbolic");
      box.pack_start(refresh_label, false, false, 0);
    } else {
      var nothing_label = new Ros.I3BlocksInfoLabel("Nothing to do!", "object-select-symbolic");
      box.pack_start(nothing_label, false, false, 0);
    }

    var btn_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 10);
    box.pack_start(btn_box, false, false, 0);

    var cancel_btn = new Gtk.Button.with_label("Close");
    cancel_btn.clicked.connect(() => {
      this.close();
    });
    btn_box.pack_start(cancel_btn, true, true, 0);

    var update_btn = new Gtk.Button.with_label("Apply");
    Ros.StyleHelper.add_class(update_btn, "suggested-action");
    update_btn.clicked.connect(() => {
      update_btn.sensitive = false;
      update();
    });
    btn_box.pack_start(update_btn, true, true, 0);

    if ((install_blocks.length == 0) && (remove_blocks.length == 0)) {
      update_btn.sensitive = false;
    }

    show_all();
  }

  private void style_window() {
    set_destroy_with_parent(true);
    set_transient_for(parent_window);
    set_keep_above(true);
    modal = true;

    var hb = new Gtk.HeaderBar();
    hb.title = "Summary Of Updates";
    set_titlebar(hb);

    icon = new Gdk.Pixbuf.from_resource("/logo/regolith-on-stage.svg");
  }

  public void update() {
    update_install();
  }

  private void update_install() {
    int install_pid;

    if (install_blocks.length > 0)
    {
      install_label.processing();
      Process.spawn_async ("/",
  			to_argv("install", install_blocks),
  			Environ.get(),
  			SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD,
  			null,
  			out install_pid);

  		ChildWatch.add (install_pid, (pid, exit_status) => {
  			Process.close_pid (pid);

        if (exit_status != 0) {
          install_label.failure("Failed to install blocks. Aborting.");
          invalidate_update(install_blocks);
          if (remove_blocks.length > 0) {
            remove_label.failure("Aborting.");
            invalidate_update(remove_blocks);
          }
        } else {
          install_label.success("Blocks installed successfully.");
          validate_update(install_blocks);
          parent_window.desktop_config_box.parse_vars_from_new_i3blocks();
          if (remove_blocks.length > 0) {
            update_remove();
          } else {
            update_refresh();
          }
        }
  		});
    } else if (remove_blocks.length > 0) {
      update_remove();
    }
  }

  private void update_remove() {
    int remove_pid;

    remove_label.processing();
    Process.spawn_async ("/",
			to_argv("purge", remove_blocks),
			Environ.get(),
			SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD,
			null,
			out remove_pid);

		ChildWatch.add (remove_pid, (pid, exit_status) => {
			Process.close_pid (pid);

      if (exit_status != 0) {
        remove_label.failure("Failed to remove blocks. Aborting.");
        invalidate_update(remove_blocks);
        if (install_blocks.length > 0) { update_refresh(); }
      } else {
        remove_label.success("Blocks removed successfully.");
        validate_update(remove_blocks);
        update_refresh();
      }
		});
  }

  private void update_refresh() {
    int refresh_pid;

    refresh_label.processing();
    Process.spawn_async ("/",
			{ "/usr/bin/regolith-look", "refresh" },
			Environ.get(),
			SpawnFlags.SEARCH_PATH | SpawnFlags.DO_NOT_REAP_CHILD,
			null,
			out refresh_pid);

    ChildWatch.add (refresh_pid, (pid, exit_status) => {
			Process.close_pid (pid);

      if (exit_status != 0) {
        refresh_label.failure("Failed to refresh desktop");
      } else {
        refresh_label.success("Desktop refreshed successfully.", true);
      }
		});
  }

  private void validate_update(Array<Ros.I3BlockData> blocks) {
    for (int i = 0; i < blocks.length; i++) {
      var b = blocks.index(i);
      b.installed_init = b.installed_to_be;
    }
  }

  private void invalidate_update(Array<Ros.I3BlockData> blocks) {
    for (int i = 0; i < blocks.length; i++) {
      var b = blocks.index(i);
      b.installed_to_be = b.installed_init;
      b.row.blk_switch.state = b.installed_init;
    }
    list.block_list.invalidate_sort();
    list.bar_preview.preview();
  }
  private string[] to_argv(string action, Array<Ros.I3BlockData> blocks) {
    string[] cmd = {};

    cmd += "pkexec";
    cmd += "apt";
    cmd += action;
    cmd += "-y";

    for (int i = 0; i < blocks.length; i++) {
      cmd += "i3xrocks-" + blocks.index(i).name;
    }

    return cmd;
  }
}

public class Ros.I3BlocksInfoLabel : Gtk.Box {
  public Gtk.Label    label;
  public Gtk.Image    icon;
  public Gtk.Spinner  spinner;

  public I3BlocksInfoLabel (string title, string icon_name) {
    style();
    label = new Gtk.Label(title);
    add_icon(icon_name);
    pack_start(label, false, false, 0);
  }

  public I3BlocksInfoLabel.with_blocks (string title, Array<Ros.I3BlockData> blocks, string icon_name) {
    style();

    label = new Gtk.Label(title + "\n");
    for (int i = 0; i < blocks.length; i++) {
      var b = blocks.index(i);
      label.label += (i + 1).to_string() + ". " + b.name + "\n";
    }

    add_icon(icon_name);

    pack_start(label, false, false, 0);
  }

  public void processing() {
    remove(icon);

    spinner = new Gtk.Spinner();
    spinner.visible = true;
    spinner.start();
    spinner.valign = Gtk.Align.START;
    pack_start(spinner, false, false, 0);
    reorder_child(spinner, 0);
  }

  public void success(string s, bool replace = false) {
    if (replace) {
      label.label = s;
    } else {
      label.label += s;
    }
    remove(spinner);
    icon.set_from_icon_name("object-select-symbolic", Gtk.IconSize.MENU);
    pack_start(icon, false, false, 0);
    reorder_child(icon, 0);
  }

  public void failure(string s, bool replace = false) {
    if (replace) {
      label.label = s;
    } else {
      label.label += s;
    }
    remove(spinner);
    icon.set_from_icon_name("window-close-symbolic", Gtk.IconSize.MENU);
    pack_start(icon, false, false, 0);
    reorder_child(icon, 0);
  }

  private void add_icon(string icon_name) {
    icon = new Gtk.Image.from_icon_name(icon_name, Gtk.IconSize.MENU);
    icon.valign = Gtk.Align.START;
    pack_start(icon, false, false, 0);
  }

  private void style() {
    orientation = Gtk.Orientation.HORIZONTAL;
    spacing = 5;
  }
}
