public class Ros.BarPreview : Gtk.Box {
  public Ros.I3Blocks           parent;
  public Gtk.Box                image_box;
  public Array<Ros.I3BlockData> blocks;

  public BarPreview(Ros.I3Blocks parent) {
    this.parent = parent;
    orientation = Gtk.Orientation.VERTICAL;
    spacing = 5;

    var heading = new Gtk.Label(null);
    heading.halign = Gtk.Align.START;
    Ros.StyleHelper.add_class(heading, "i3blocks_header");
    heading.label = "i3Bar Preview";
    pack_start(heading, false, false, 0);

    parent.help.register_step("preview", new Ros.HelpStep(this, "Here you will can a real time preview of your bar."));
  }

  public void init(Array<Ros.I3BlockData> b) {
    blocks = b;

    image_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 15);
    pack_start(image_box, false, false, 0);

    preview();
  }

  public void preview(string highlight_block = "") {
    Gtk.Image hightlight_img = null;

    // Replace image box
    remove(image_box);
    image_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 15);
    image_box.halign = Gtk.Align.CENTER;
    Ros.StyleHelper.add_class(image_box, "i3blocks_bar_preview_img_box");
    pack_start(image_box, false, false, 0);

    // Add block images
    for (int i = 0; i < blocks.length; i++) {
      var b = blocks.index(i);
      if (b.installed_to_be) {
        var img = new Gtk.Image.from_resource("/blocks/" + b.name + ".png");
        image_box.pack_start(img, false, false, 0);
        if (b.name == highlight_block) {
          hightlight_img = img;
        }
      }
    }
    image_box.show_all();

    // Highlight newly added block
    if (hightlight_img != null) {
      Ros.StyleHelper.add_class(hightlight_img, "i3blocks_highlighted_block_img");
    }
  }
}
