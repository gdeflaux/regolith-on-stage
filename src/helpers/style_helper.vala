public class Ros.StyleHelper {
  public static void load_stylesheet_from_resource(string stylesheet) {
    var css_provider = new Gtk.CssProvider();
    css_provider.load_from_resource(stylesheet);
    var screen_context = new Gtk.StyleContext();
    screen_context.add_provider_for_screen(Gdk.Screen.get_default(), css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);
  }

  public static Gtk.StyleContext add_class(Gtk.Widget w, string class_name) {
    var css_context = w.get_style_context();
    css_context.add_class(class_name);
    return css_context;
  }

  public static Gtk.StyleContext remove_class(Gtk.Widget w, string class_name) {
    var css_context = w.get_style_context();
    css_context.remove_class(class_name);
    return css_context;
  }
}
